const TestAPI = require('./testAPI');

const createDataSources = () => ({
  testAPI: new TestAPI()
});

module.exports = createDataSources;