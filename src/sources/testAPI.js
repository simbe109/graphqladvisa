const { RESTDataSource } = require('apollo-datasource-rest');
const mockData = require('./test-data.json')

class TestAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://test-api.com/';
  }

  async getBook(id) {
    const books = this.context.marketUnit === 'fi' ? mockData.testBooksFI : mockData.testBooksSE
    return books.find(book => book.id === id)
  }

  async getAuthor(id) {
    const authors = this.context.marketUnit === 'fi' ? mockData.testAuthorsFI : mockData.testAuthorsSE  
    return authors.find(author => author.id === id)
  }

  async getBooksOnAuthor(id) {
    const books = this.context.marketUnit === 'fi' ? mockData.testBooksFI : mockData.testBooksSE
    return books.filter(bb => bb.authorId === id);
  }
}

module.exports = TestAPI