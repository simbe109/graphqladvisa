

const validMarketCodes = {
  sv: 'sv',
  fi: 'fi'
}

const getMarketUnit = (code = 'sv') => validMarketCodes[code] || validMarketCodes.sv

const createContext = ({ req, connection }) => {
  if(connection) {
    return connection.context;
  }
  const marketUnit = getMarketUnit(req.headers['market-unit']);
  return {
    marketUnit
  }
}

module.exports = createContext;