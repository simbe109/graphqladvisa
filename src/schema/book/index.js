const { PubSub } = require('apollo-server');
const gql = require('graphql-tag');

const pubsub = new PubSub();

const typeDefs = gql`
  extend type Query {
    book(id: Int!): Book
  }

  extend type Mutation {
    addBook(input: BookInput): Book
  }

  extend type Subscription {
    bookAdded: Book
  }

  input BookInput {
    title: String
    authorId: Int
  }

  type Book {
    title: String
    author: Author
  }

`;

const BOOK_ADDED = 'BOOK_ADDED';

const resolvers = {
  Query: {
    book: (_, args, context) => {
      return context.dataSources.testAPI.getBook(args.id);
    }
  },
  Mutation: {
    addBook: (_, { input }, ___) => {
      console.log('A book was added with data', input);
      pubsub.publish(BOOK_ADDED, { bookAdded: input });
      return input;
    }
  },
  Subscription: {
    bookAdded: {
      subscribe: () => pubsub.asyncIterator([BOOK_ADDED]),
    }
  },
  Book: {
    author: (parent, __, context) => {
      console.log('parent authorId', parent.authorId);
      return context.dataSources.testAPI.getAuthor(parent.authorId);
    }
  }
};

module.exports = {
  typeDefs,
  resolvers
} 