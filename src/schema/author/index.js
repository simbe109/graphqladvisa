const gql = require('graphql-tag');
const mockData = require('../../sources/test-data.json')


// author.js
const typeDefs = gql`
  extend type Query {
    author(id: Int!): Author
  }
  type Author {
    id: Int!
    firstName: String
    lastName: String
    books: [Book]
  }
`;

const resolvers = {
  Query: {
    author: (_, args, context) => {
      return context.dataSources.testAPI.getAuthor(args.id);
    }
  },
  Author: {
    books: (parent, __, context) => {
      return context.dataSources.testAPI.getBooksOnAuthor(parent.id);
    } 
  }
};

module.exports = {
  typeDefs,
  resolvers
}
