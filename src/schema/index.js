const gql = require('graphql-tag');
const merge = require('lodash.merge');
const { makeExecutableSchema } = require('graphql-tools');
const { typeDefs: Author, resolvers: authorResolvers } = require('./author');
const { typeDefs: Book, resolvers: bookResolvers } = require('./book');

const Query = gql`
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  type Subscription {
    _empty: String
  }
`;

const resolvers = {};

module.exports = makeExecutableSchema({
  typeDefs: [ Query, Book, Author ],
  resolvers: merge(resolvers, authorResolvers, bookResolvers),
});