const { ApolloServer } = require('apollo-server');
const schema = require('./src/schema');
const createContext = require('./src/context')
const createDataSources = require('./src/sources');

const server = new ApolloServer({ 
  schema, 
  context: createContext,
  dataSources: createDataSources,
  tracing: true,
  formatError: (err) => {
    console.log('## GRAHQL ERROR ##');
    console.log(err);
    console.log('#####');
    return err;
  },
});

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});